<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('route-1','HomeController@route1')->middleware('auth','role:superadmin')->name('Route 1');
Route::get('route-2','HomeController@route2')->middleware('auth','role:admin,superadmin')->name('Route 2');
Route::get('route-3','HomeController@route3')->middleware('auth','role:guest,admin,superadmin')->name('Route 3');
