@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                    <ul>
                    <li>Name: {{auth()->user()->name}}</li>
                    <li>Email: {{auth()->user()->email}}</li>
                    <li>Role: {{auth()->user()->role}}</li>
                    </ul>
                    <p>
                        <a href="/route-1" class="btn btn-success">Route 1</a>
                        <a href="/route-2" class="btn btn-success">Route 2</a>
                        <a href="/route-3" class="btn btn-success">Route 3</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
